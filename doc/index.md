title: The 66-tools Suite: index
author: Eric Vidal <eric@obarun.org>

[Software](https://web.obarun.org/software)

[obarun.org](https://web.obarun.org)

# What is 66-tools

Sixty-six-tools is a collection of helpers tools to accomplish various and repetitive tasks in service scripts. Some utilities are language [execline](https://skarnet.org/software/execline) specific (usually named with `execl-` prefix) where other can be used on classic shell.

## Installation

### Requirements

Please refer to the [INSTALL.md](https://framagit.org/Obarun/66-tools) file for details.

### Licensing

*66-tools* is free software. It is available under the [ISC license](http://opensource.org/licenses/ISC).

### Upgrade

See [changes](upgrade.html) between version.

---

## Commands

- [execl-cmdline](execl-cmdline.html)

- [execl-subuidgid](execl-subuidgid.html)

- [execl-toc](execl-toc.html)

- [66-clock](66-clock.html)

- [66-getenv](66-getenv.html)

- [66-gnwenv](66-gnwenv.html)

- [66-ns](66-ns.html)

- [66-olexec](66-olexec.html)

- [66-writenv](66-writenv.html)

- [66-which](66-which.html)

- [66-yeller](66-yeller.html)
